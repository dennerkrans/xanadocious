const mix = require('laravel-mix')

mix.setPublicPath('dist')

mix.setResourceRoot('src')

mix.browserSync('xanadocious.abc')
mix.js('./src/main.js', './dist/')
