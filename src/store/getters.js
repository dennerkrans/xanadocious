/**
 * character
 * @param {state, getters, rootState, rootGetters}
 * @return state.character
 */
export const character = state => {
  return state.character
}
