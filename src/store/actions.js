/**
 * character
 * @param { state, commit, rootState } param
 * @param {*} character
 */
export const character = ({ commit }, character) => {
  return commit('character', character)
}
