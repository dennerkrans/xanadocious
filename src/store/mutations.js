/**
 * character mutation
 * @param character
 */
export const character = (state, character) => {
  return (state.character = character)
}
